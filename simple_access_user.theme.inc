<?php

/**
 * @see theme_simple_access_page_overview_list
 */
function theme_simple_access_user_page_overview_list(&$form) {
  drupal_add_tabledrag('sa-group-list', 'order', 'sibling', 'sa-group-weight');

  $output = '';
  $options = array(
    'header' => array(t('Group'), t('Roles'), t('Users'), t('Weight'), t('Operations')),
    'rows' => array(),
    'attributes' => array('id' => 'sa-group-list'),
  );

  foreach (element_children($form['form'], TRUE) as $gid) {
    $options['rows'][] = array(
      'data' => array(
        drupal_render($form['form'][$gid]['name']),
        array('data' => drupal_render($form['form'][$gid]['roles']), 'class' => 'sa-group-roles'),
        array('data' => drupal_render($form['form'][$gid]['users']), 'class' => 'sa-group-users'),
        drupal_render($form['form'][$gid]['weight']),
        drupal_render($form['form'][$gid]['ops']),
      ),
      'class' => array('draggable'),
    );
  }

  $output .= theme('table', $options);

  return $output;
}